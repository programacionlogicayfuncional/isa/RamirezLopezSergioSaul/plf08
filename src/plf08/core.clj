(ns plf08.core
  (:gen-class)
(:require [plf08.Cuadro :as t]
          [plf08.vector :as f]
          [clojure.string :as string]))

(defn cifrar
  [r s]
  (t/escribir s (string/join (map t/tabla (t/leer r)))))
(cifrar "resources/cifrar.txt" "resources/cifrado.txt")

(defn descifrar
  [r s]
  (t/escribir s (string/join (map t/tabla (vec (map (partial apply str)
                                                    (partition-all 2 (t/leer r))))))))
(descifrar "resources/descifrar.txt" "resources/descifrado.txt")






(defn funcion-1
  [y f]
  (str (nth (nth (vec (map vec (partition-all (count  y) f))) 0) 3) 
       (nth (nth (vec (map vec (partition-all (count  y) f))) 1) 3)))  

(funcion-1 "cancio" "parafraseando") 


(defn x
  [y f]
  (map vec (partition-all (count (f/vecf y)) f))) 

(zipmap(first(x "cancio" "holamundh")) (f/vecf "cancio"))


(defn busca
  [y f]
  (apply str (map first (x y f))))  


(map vec (partition-all (count (f/vecf "cancio")) "Parafraseando"))

(x "cancio" "Parafraseando")
(x "cancio" "holamundh")
(f/vecf "saul")
([\P \a \r \a \f \r] [\a \s \e \a \n \d] [\o])


(defn -main
  ([a b c] [(if (= a "cifrar")
              (cifrar b c)
              (descifrar b c))])
  ([a b c] [(if (= a "cifrar")
                (cifrar b c)
                (descifrar b c))]))

